#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <dirent.h>
#include <ctype.h>

#define MAX_CMD_SIZE 100
#define MAX_TOKEN_SIZE 10000
#define PORT 5555

struct UserPass{
	char name[MAX_TOKEN_SIZE];
	char pass[MAX_TOKEN_SIZE];
};

int checkUserPass(char *username, char *pass){
	struct UserPass client;
	int exist = 0;

	FILE *fUser;
	fUser = fopen("../database/databases/user.txt", "rb");
	if (fUser == NULL) {
        perror("Warning: Failed to open the file");
        return 1;
    }

	while(1){
		fscanf(fUser, "%s : %s\n", client.name, client.pass);
		if (strcmp(client.name, username) == 0 && strcmp(client.pass, pass) == 0){
			exist = 1;
			break;
		}
	}

	fclose(fUser);

	if (exist == 0){
		printf("Permission Denied: wrong username or password\n");
		return 0;
	}else{
		return 1;
	}
}

void writeLog(const char *cmd, const char *username) {
	char temp[100];
	sscanf(cmd, "%[^;]", temp);

    time_t current_time = time(NULL);
    struct tm *time_info = localtime(&current_time);

    char log_entry[1000];
    snprintf(log_entry, sizeof(log_entry), "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s\n",
             time_info->tm_year + 1900, time_info->tm_mon + 1, time_info->tm_mday,
             time_info->tm_hour, time_info->tm_min, time_info->tm_sec, username, temp);

    FILE *log_file = fopen("../database/log/log.txt", "ab");
    if (log_file == NULL) {
        perror("Failed to open the log file");
        exit(1);
    }

    fputs(log_entry, log_file);
    fclose(log_file);
}

int main(int argc, char *argv[]){
	int allowed = 0;
	int id_user = geteuid();
	char used_db[1000];

	if (id_user == 0)
		allowed = 1;
	else{
		allowed = checkUserPass(argv[2], argv[4]);
	}

	if (allowed == 0){
		exit(1);
	}

	int client_socket, res;
	struct sockaddr_in serverAddr = {0};
	char buff[32000];

	client_socket = socket(AF_INET, SOCK_STREAM, 0);
	if(client_socket < 0){
    	perror("Warning: Error in connection");
		exit(1);
	}
	printf("Success: Client Socket is created\n");

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	inet_pton(AF_INET, "127.0.0.1", &(serverAddr.sin_addr));

	res = connect(client_socket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
	if (res < 0){
		perror("Warning: Error connection");
    	exit(1);
	}
	printf("Success: Connected to Server\n");

  	while(1){
		printf("Client:  ");
		char input[MAX_TOKEN_SIZE];
        char temp[MAX_TOKEN_SIZE];
        char cmd[MAX_CMD_SIZE][MAX_TOKEN_SIZE];
        char *token;
        int i = 0;
		scanf(" %[^\n]s", input);
		strcpy(temp, input);
		token = strtok(input, " ");

		while (token != NULL){
			strcpy(cmd[i], token);
			i++;
			token = strtok(NULL, " ");
		}

		int expcmd = 0;
		if(strcmp(cmd[0], "CREATE") == 0){
			if(strcmp(cmd[1], "USER") == 0 && strcmp(cmd[3], "IDENTIFIED") == 0 && strcmp(cmd[4], "BY") == 0){
				char pw[MAX_TOKEN_SIZE];
				sscanf(cmd[5], "%[^;]", pw);
				snprintf(buff, sizeof buff, "createUser %s %s %d", cmd[2], pw, id_user);
				send(client_socket, buff, strlen(buff), 0);
			}else if (strcmp(cmd[1], "DATABASE") == 0){
				char db[MAX_TOKEN_SIZE];
				sscanf(cmd[2], "%[^;]", db);
				snprintf(buff, sizeof buff, "createDatabase %s %s %d", db, argv[2], id_user);
				send(client_socket, buff, strlen(buff), 0);
			}else if (strcmp(cmd[1], "TABLE") == 0){
				char tb[MAX_TOKEN_SIZE];
				sscanf(temp, "%[^;]", tb);
				snprintf(buff, sizeof buff, "createTable %s", tb);
				send(client_socket, buff, strlen(buff), 0);
			}
		}else if (strcmp(cmd[0], "GRANT") == 0 && strcmp(cmd[1], "PERMISSION") == 0 && strcmp(cmd[3], "INTO") == 0){
			char gr[MAX_TOKEN_SIZE];
			sscanf(cmd[4], "%[^;]", gr);
			snprintf(buff, sizeof buff, "grantPermission %s %s %d", cmd[2], gr, id_user);
			send(client_socket, buff, strlen(buff), 0);
		}else if (strcmp(cmd[0], "USE") == 0){
			char us[MAX_TOKEN_SIZE];
			sscanf(cmd[1], "%[^;]", us);
			snprintf(buff, sizeof buff, "useDatabase %s %s %d", us, argv[2], id_user);
			send(client_socket, buff, strlen(buff), 0);
		}else if (strcmp(cmd[0], "DROP") == 0){
			if (strcmp(cmd[1], "DATABASE") == 0){
				char dd[MAX_TOKEN_SIZE];
				sscanf(cmd[2], "%[^;]", dd);
				snprintf(buff, sizeof buff, "dropDatabase %s %s", dd, argv[2]);
				send(client_socket, buff, strlen(buff), 0);
			}else if (strcmp(cmd[1], "TABLE") == 0){
				char dt[MAX_TOKEN_SIZE];
				sscanf(cmd[2], "%[^;]", dt);
				snprintf(buff, sizeof buff, "dropTable %s %s", dt, argv[2]);
				send(client_socket, buff, strlen(buff), 0);
			}else if (strcmp(cmd[1], "COLUMN") == 0){
				char dc[MAX_TOKEN_SIZE];
				sscanf(cmd[4], "%[^;]", dc);
				snprintf(buff, sizeof buff, "dropColumn %s %s %s", cmd[2], dc, argv[2]);
				send(client_socket, buff, strlen(buff), 0);
			}
		}else if (strcmp(cmd[0], "INSERT") == 0 && strcmp(cmd[1], "INTO") == 0){
			char in[MAX_TOKEN_SIZE];
			sscanf(temp, "%[^;]", in);
			snprintf(buff, sizeof buff, "insert %s", in);
			send(client_socket, buff, strlen(buff), 0);
		}else if (strcmp(cmd[0], "UPDATE") == 0){
			char up[MAX_TOKEN_SIZE];
			sscanf(temp, "%[^;]", up);
			snprintf(buff, sizeof buff, "update %s", up);
			send(client_socket, buff, strlen(buff), 0);
		}else if (strcmp(cmd[0], "DELETE") == 0){
			char de[MAX_TOKEN_SIZE];
			sscanf(temp, "%[^;]", de);
			snprintf(buff, sizeof buff, "delete %s", de);
			send(client_socket, buff, strlen(buff), 0);
		}else if (strcmp(cmd[0], "SELECT") == 0){
			char se[MAX_TOKEN_SIZE];
			sscanf(temp, "%[^;]", se);
			snprintf(buff, sizeof buff, "select %s", temp);
			send(client_socket, buff, strlen(buff), 0);
		}else if (strcmp(cmd[0], "exit") != 0){
			expcmd = 1;
			char warn[] = "Invalid Command";
			send(client_socket, warn, strlen(warn), 0);
		}

		char client[MAX_TOKEN_SIZE];
		strcpy(client, (id_user == 0) ? "root" : argv[2]);
		if (!expcmd) {
			writeLog(temp, client);
		}

		if (strcmp(cmd[0], "exit") == 0){
			send(client_socket, cmd[0], strlen(cmd[0]), 0);
			close(client_socket);
			printf("Exit: Disconnected from server.\n");
			exit(1);
		}else{
			memset(buff, 0, sizeof(buff));
			if (recv(client_socket, buff, 1024, 0) < 0){
				printf("Warning: Error in receiving data.\n");
			}
			else{
				printf("Database:  %s\n", buff);
			}
		}
  	}

  	return 0;
}