#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <dirent.h>
#include <ctype.h>

#define MAX_CMD_SIZE 100
#define MAX_TOKEN_SIZE 10000
#define PORT 5555

struct table{
    int tot_column;
    char type[MAX_CMD_SIZE][MAX_TOKEN_SIZE];
    char data[MAX_CMD_SIZE][MAX_TOKEN_SIZE];
}; 

struct permission{
    char name[MAX_TOKEN_SIZE];
    char pass[MAX_TOKEN_SIZE];
};

struct permissionDatabase{
    char database[MAX_TOKEN_SIZE];
    char name[MAX_TOKEN_SIZE];
};

int checkUser(char *uname){
    FILE *fp = fopen("../database/databases/user.txt", "rb");
    if (fp == NULL) {
        perror("Failed to open the file");
        exit(1);
    }

    struct permission user;

    while (1){
        fscanf(fp, "%s : %s\n", user.name, user.pass);
        if (strcmp(user.name, uname) == 0){
            return 1;
        }if (feof(fp)){
            fclose(fp);
        }
    }
    return 0;
}

void createUser(char *name, char *pass){
    struct permission user;
    strcpy(user.name, name);
    strcpy(user.pass, pass);

    printf("User created:%s with password:%s\n", user.name, user.pass);

    char path[] = {"databases/user.txt"};

    FILE *fp = fopen(path, "ab");
    fprintf(fp, "%s : %s\n", user.name, user.pass);
    fclose(fp);
}

int checkPermission(char *name, char *database){
    FILE *fp = fopen("../database/databases/permission.txt", "rb");
    struct permissionDatabase user;
    while(1){
        fscanf(fp, "%s : %s\n", user.name, user.database);
        if(strcmp(user.name, name) == 0 && strcmp(user.database, database) == 0){
            printf("%s has access to database %s\n", name, database);
            return 1;
        }        
        if(feof(fp)){
            fclose(fp);
        }
    }
    return 0;
}

void addPermission(char *name, char *database){
    struct permissionDatabase user;
    strcpy(user.name, name);
    strcpy(user.database, database);

    char path[] = {"databases/permission.txt"};
    FILE *fp = fopen(path, "ab");
    fprintf(fp, "%s : %s\n", user.name, user.database);
    printf("Permission added: %s now can access database %s\n", user.name, user.database);
    fclose(fp);
}

int main(){
    int db_socket, res;
    struct sockaddr_in serverAddr = {0};

    int new_db_socket;
    struct sockaddr_in newAddr;

    socklen_t addr_size;

    char buff[1024];
    pid_t childpid;

    db_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(db_socket < 0){
    	perror("Warning: Error in connection");
		exit(1);
	}
	printf("Success: Socket is created\n");

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
	inet_pton(AF_INET, "127.0.0.1", &(serverAddr.sin_addr));

    res = bind(db_socket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
    if (res < 0){
		perror("Warning: Error in binding");
        exit(1);
    }
    printf("Success: Bind to port %d\n", PORT);

    if (listen(db_socket, 10) == 0){
        printf("Success: Listening....\n");
    }else{
        printf("Warning: Error in binding.\n");
    }

    while(1){
        new_db_socket = accept(db_socket, (struct sockaddr *)&newAddr, &addr_size);

        if (new_db_socket < 0){
            exit(1);
        }
        printf("Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

        if ((childpid = fork()) == 0){
            close(db_socket);
            while (1){
                recv(new_db_socket, buff, 1024, 0);
                char *token;
                char temp[32000];
                strcpy(temp, buff);
                char cmd[MAX_CMD_SIZE][MAX_TOKEN_SIZE];
                token = strtok(temp, " ");
                int i = 0;
                char used_db[MAX_CMD_SIZE];

                while (token != NULL){
                    strcpy(cmd[i], token);
                    i++;
                    token = strtok(NULL, " ");
                }
                
                if (strcmp(cmd[0], "createUser") == 0){
                    if (strcmp(cmd[3], "0") == 0){
                        createUser(cmd[1], cmd[2]);
                    }else{
                        char warn[] = "Permission Denied: Can't create User";
                        send(new_db_socket, warn, strlen(warn), 0);
		                memset(buff, 0, sizeof(buff));
                    }
                }else if (strcmp(cmd[0], "useDatabase") == 0){
                    if (strcmp(cmd[3], "0") != 0){
                        int allowed = checkPermission(cmd[2], cmd[1]);
                        if (allowed != 1){
                            char warn[] = "Permission Denied: You're not allowed to access database";
                            send(new_db_socket, warn, strlen(warn), 0);
		                    memset(buff, 0, sizeof(buff));
                        }else{
                            strncpy(used_db, cmd[1], sizeof(cmd[1]));
                            char warn[] = "Access_database : Allowed";
                            printf("used_db = %s\n", used_db);
                            send(new_db_socket, warn, strlen(warn), 0);
                            memset(buff, 0, sizeof(buff));
                        }
                    }
                }else if (strcmp(cmd[0], "grantPermission") == 0){
                    if (strcmp(cmd[3], "0") == 0){
                        int exist = checkUser(cmd[2]);
                        if (exist == 1){
                            addPermission(cmd[2], cmd[1]);
                        }else{
                            char warn[] = "Warning: User Not Found";
                            send(new_db_socket, warn, strlen(warn), 0);
		                    memset(buff, 0, sizeof(buff));
                        }
                    }else{
                        char warn[] = "Permission Denied: Can't grant permission";
                        send(new_db_socket, warn, strlen(warn), 0);
		                memset(buff, 0, sizeof(buff));
                    }
                }else if (strcmp(cmd[0], "createDatabase") == 0){
                    char path[20000];
                    snprintf(path, sizeof path, "databases/%s", cmd[1]);
                    printf("location = %s, name = %s , database = %s\n", path, cmd[2], cmd[1]);
                    mkdir(path, 0777);
                    addPermission(cmd[2], cmd[1]);
                }else if (strcmp(cmd[0], "createTable") == 0){
                    
                }else if (strcmp(cmd[0], "dropDatabase") == 0){
                    int allowed = checkPermission(cmd[2], cmd[1]);

                    if (allowed != 1){
                        char warn[] = "Permission Denied: You're not allowed to access database";
                        send(new_db_socket, warn, strlen(warn), 0);
                        memset(buff, 0, sizeof(buff));
                        continue;
                    }else{
                        char drop[20000];
                        snprintf(drop, sizeof drop, "rm -r databases/%s", cmd[1]);
                        system(drop);
                        char warn[] = "Success: Database Has Been Removed";
                        send(new_db_socket, warn, strlen(warn), 0);
                        memset(buff, 0, sizeof(buff));
                    }
                }else if (strcmp(cmd[0], "dropTable") == 0){
                    if (used_db[0] == '\0'){
                        strcpy(used_db, "Error: Database has not been selected");
                        send(new_db_socket, used_db, strlen(used_db), 0);
                        memset(buff, 0, sizeof(buff));
                        continue;
                    }
                    char drop[20000];
                    snprintf(drop, sizeof drop, "databases/%s/%s", used_db, cmd[1]);
                    remove(drop);
                    char warn[] = "Success: Table has been deleted";
                    send(new_db_socket, warn, strlen(warn), 0);
                    memset(buff, 0, sizeof(buff));
                }else if (strcmp(cmd[0], "dropColumn") == 0){
                    
                }else if (strcmp(cmd[0], "insert") == 0){
                    
                }else if (strcmp(cmd[0], "update") == 0){
                    
                }else if (strcmp(cmd[0], "delete") == 0){
                    
                }else if (strcmp(cmd[0], "select") == 0){
                    
                }
                
                if (strcmp(buff, "exit") == 0){
                    printf("Exit: Disconnected from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
                    exit(1);
                }else{
                    printf("Client:  %s\n", buff);
                    send(new_db_socket, buff, strlen(buff), 0);
                    memset(buff, 0, sizeof(buff));
                }
            }
        }
    }
    close(new_db_socket);

    return 0;
}
