# Base image
FROM ubuntu:20.04

# Set the working directory
WORKDIR /app

# Copy the source code into the container
COPY . /app

# Install the required packages
RUN apt-get update && apt-get install -y gcc

# Compile the server and client programs
RUN gcc -o server database.c
RUN gcc -o client client.c

# Expose the port used by the server
EXPOSE 12345

# Run the server by defaults
CMD ["./server"]