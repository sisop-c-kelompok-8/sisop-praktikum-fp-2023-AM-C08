# Client
Pada file client.c akan menghandle 2 user, yaitu root dan user yang akan terdaftar pada saat melakukan input yang nantinya user terdaftar tersebut dilist ke dalam user.txt


```
int main(int argc, char* argv[]) {
    //membuat socket
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        perror("Gagal membuat socket");
        exit(EXIT_FAILURE);
    }

    //set up socket ke port 8888
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(SERVER_PORT);
    if (inet_pton(AF_INET, SERVER_IP, &(server_address.sin_addr)) <= 0) {
        perror("Alamat server tidak valid");
        exit(EXIT_FAILURE);
    }

    //connect ke socket
    if (connect(sock, (struct sockaddr*)&server_address, sizeof(server_address)) < 0) {
        perror("Koneksi ke server gagal");
        exit(EXIT_FAILURE);
    }

    //mendapatkan id user
    uid_t id = getuid();

    char command[10000];
    char full_command[12002];
```
- **int main(int argc, char* argv[])** : Ini adalah fungsi utama yang dijalankan ketika program ini dijalankan. Argumen argc dan argv digunakan untuk menerima argumen baris perintah.

- **int sock = socket(AF_INET, SOCK_STREAM, 0)** : Membuat socket menggunakan fungsi **socket()**. Parameter AF_INET menandakan bahwa kita menggunakan **IPv4**, **SOCK_STREAM** menunjukkan bahwa kita menggunakan TCP (bukan UDP), dan parameter terakhir (0) untuk protokol default (yang dalam hal ini adalah TCP).

- Kondisi **if (sock == -1)** menguji apakah pembuatan socket berhasil. Jika gagal (sock akan bernilai -1), pesan kesalahan ditampilkan dan program diakhiri.

- **struct sockaddr_in server_address** : Mendeklarasikan struktur alamat untuk koneksi.

- **server_address.sin_family** = AF_INET : Menetapkan jenis alamat ke IPv4.

- **server_address.sin_port** = htons(SERVER_PORT) : Menetapkan port server. Fungsi htons() digunakan untuk mengubah format number dari host byte order ke network byte order. Nilai dari SERVER_PORT belum ditentukan dalam kode yang diberikan.

- Fungsi **inet_pton()** digunakan untuk mengubah alamat IP dari format teks menjadi format biner, dan menyimpannya ke **server_address.sin_addr**. Jika alamat IP tidak valid atau konversi gagal, pesan kesalahan ditampilkan dan program diakhiri.

- Fungsi **connect()** digunakan untuk memulai koneksi ke server menggunakan socket yang telah dibuat. Jika koneksi gagal, pesan kesalahan ditampilkan dan program diakhiri.

- **uid_t id = getuid()** : Mendapatkan user ID dari proses yang sedang berjalan.

- **char command[10000]; char full_command[12002]** : Mendeklarasikan array karakter untuk menyimpan perintah dan perintah 
lengkap. Namun, tanpa konteks lebih lanjut, belum jelas bagaimana array ini akan digunakan.

### Root melakukan login 
```
//user root
if (id == 0) {
    //dapat melakukan create user dan grant permission
    puts("Kamu bisa melakukan:");
    puts("1. CREATE USER [nama user] IDENTIFIED BY [password];");
    puts("2. GRANT PERMISSION [nama_database] INTO [nama_user];");
    puts("3. USE [nama_database];");
    puts("4. CREATE DATABASE [nama_database];");
    puts("5. CREATE TABLE [nama_tabel] ([nama_kolom] [tipe_data], ...);");
    puts("6. DROP DATABASE [nama_database];");
    puts("7. DROP TABLE [nama_table]");
    puts("8. DROP COLUMN [nama-kolom] FROM [Nama_tabel]");
    puts("9. INSERT INTO [nama_tabel] ([value], ...);");
    puts("10. UPDATE [nama_tabel] SET [kolom]=[nilai];");
    puts("11. UPDATE [nama_tabel] SET [kolom]=[nilai] WHERE [kolom]=[value];");
    puts("12. DELETE FROM [nama_tabel];");
    puts("13. DELETE FROM [nama_tabel] WHERE [kolom]=[value];");
    puts("14. SELECT * FROM [nama_tabel];");
    puts("15. SELECT [Nama_kolom] FROM [nama_tabel];");
    puts("16. SELECT * FROM [nama_tabel] WHERE [kolom]=[value];");
    puts("17. SELECT [nama_kolom] FROM [nama_tabel] WHERE [kolom]=[value];");
    puts("18. EXIT");

    puts("Masukkan command yang kamu inginkan:");

    while (1) {
        //mendapatkan kalimat dari terminal
        fgets(command, sizeof(command), stdin);
        
        //cek apakah isi dari kalimat tersebut mengandung kata "EXIT"
        if(strstr(command, "EXIT") != NULL){
            //menambahkan keterangan ke server bahwa yang masuk adalah user root
            //contoh perintah: "root" EXIT
            sprintf(full_command, "\"root\" %s", command);
            printf("Full command: %s\n", full_command);

            //mengirim perintah melalui socket ke server
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Gagal mengirim data pengguna ke server");
                exit(EXIT_FAILURE);
            }

            return 0;
        }
        else{

            //memasukkan command selain "EXIT" dan menambahkan keterangan user "root"
            sprintf(full_command, "\"root\" %s", command);

            printf("Full command: %s\n", full_command);
            
            //mengirim perintah melalui socket
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Gagal mengirim data pengguna ke server");
                exit(EXIT_FAILURE);
            }

            //menerima respon dari server
            char server_response[2000];
            if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
                perror("Gagal menerima respons dari server");
                exit(EXIT_FAILURE);
            }

            printf("Respon dari server: %s\n", server_response);
        }
    }
} 
```

- **if (id == 0)** : Menyatakan bahwa pengguna adalah root user, karena root user memiliki user ID 0.

- **puts("Kamu bisa melakukan:")** dan seterusnya: Menampilkan daftar perintah yang bisa dilakukan oleh root user. Ini termasuk membuat user baru, memberikan izin, menggunakan database, membuat database dan tabel, memodifikasi dan menghapus database, tabel, dan kolom, serta melakukan operasi dasar seperti INSERT, UPDATE, DELETE, dan SELECT pada data.

- **while (1)** : Loop tak terbatas dimulai, memungkinkan pengguna untuk terus memasukkan perintah hingga mereka memutuskan untuk keluar.

- **fgets(command, sizeof(command), stdin)** : Menerima input perintah dari pengguna.

- **if(strstr(command, "EXIT") != NULL)**: Mengecek apakah perintah dari pengguna adalah "EXIT". Jika ya, program akan mengirim perintah "EXIT" ke server dan kemudian keluar.

- **sprintf(full_command, "\"root\" %s", command)** : Menambahkan prefix "root" ke perintah untuk memberi tahu server bahwa perintah berasal dari user root.

- **if (send(sock, full_command, strlen(full_command), 0) < 0)**: Mengirim perintah ke server. Jika terjadi error saat pengiriman, program akan menampilkan pesan error dan keluar.

- **char server_response[2000]; if (recv(sock, server_response, sizeof(server_response), 0) < 0)**: Menerima respons dari server. Jika terjadi error saat menerima respons, program akan menampilkan pesan error dan keluar.

- **printf("Respon dari server: %s\n", server_response)**: Menampilkan respons yang diterima dari server.


### Root bisa membuat user baru untuk dapat mengakses server
```
//memasukkan command selain "EXIT" dan menambahkan keterangan user "root"
sprintf(full_command, "\"root\" %s", command);

printf("Full command: %s\n", full_command);

//mengirim perintah melalui socket
if (send(sock, full_command, strlen(full_command), 0) < 0) {
    perror("Gagal mengirim data pengguna ke server");
    exit(EXIT_FAILURE);
}

//menerima respon dari server
char server_response[2000];
if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
    perror("Gagal menerima respons dari server");
    exit(EXIT_FAILURE);
}

printf("Respon dari server: %s\n", server_response);

```
- **sprintf(full_command, "\"root\" %s", command)** : Ini adalah fungsi standar C untuk format string. Ini digunakan untuk menggabungkan dua string: string "root" dan command (yang merupakan perintah yang dimasukkan oleh pengguna). Output dari fungsi ini disimpan dalam variabel full_command.

- **printf("Full command: %s\n", full_command)** : Menampilkan perintah lengkap yang akan dikirimkan ke server.

- **if (send(sock, full_command, strlen(full_command), 0) < 0)** : Fungsi **send()** digunakan untuk mengirimkan data melalui socket. Data yang dikirim adalah **full_command**, dan panjang data adalah **strlen(full_command)**. Jika ada masalah dalam pengiriman data, fungsi ini akan mengembalikan nilai negatif dan program akan mencetak pesan kesalahan dan keluar.

- **char server_response[2000]** : Ini adalah deklarasi array karakter untuk menyimpan respons dari server.

- **if (recv(sock, server_response, sizeof(server_response), 0) < 0)** : Fungsi recv() digunakan untuk menerima data melalui socket. Data yang diterima disimpan dalam variabel server_response. Jika ada masalah dalam menerima data, fungsi ini akan mengembalikan nilai negatif dan program akan mencetak pesan kesalahan dan keluar.

- **printf("Respon dari server: %s\n", server_response)** : Menampilkan respons yang diterima dari server.

### Jika user bukan root dan user yang sudah masuk ke dalam file user.txt

```
//user bukan merupakan root
else {

    char user[2000], password[2000];
    int option;

    //menerima inputan dari user apa dan passwordnya apa
    while ((option = getopt(argc, argv, "u:p:")) != -1) {
        switch (option) {
            //mengambil user
            case 'u':
                strncpy(user, optarg, sizeof(user) - 1);
                break;
            //mengambil password
            case 'p':
                strncpy(password, optarg, sizeof(password) - 1);
                break;
            default:
                printf("Pilihan tidak valid\n");
                exit(EXIT_FAILURE);
        }
    }

    printf("user: %s, password: %s\n", user, password);

    //membuka users.txt yang berisi user yang telah dibuat oleh root
    FILE* file = fopen("users.txt", "r");
    int userFound = 0;
    //jika users.txt belum terbuat maka buat terlebih dahulu
    if (file == NULL) {

        system("touch users.txt");
        system("sudo chmod 777 users.txt");
        
        FILE* file2 = fopen("users.txt", "r");
        char line[100];
        //cek apakah user dan password valid
        while (fgets(line, sizeof(line), file2)) {
            line[strcspn(line, "\n")] = '\0';
            if (strstr(line, user) != NULL) {
                if (strstr(line, password) != NULL) {
                    userFound = 1;
                    break;
                }
            }
        }
        fclose(file2);
    }
    else{
        char line[100];
        while (fgets(line, sizeof(line), file)) {
            line[strcspn(line, "\n")] = '\0';
            if (strstr(line, user) != NULL) {
                if (strstr(line, password) != NULL) {
                    userFound = 1;
                    break;
                }
            }
        }

        fclose(file);
    }

    if (userFound == 0) {
        printf("User tidak ditemukan\n");
        exit(EXIT_FAILURE);
    } else {
        puts("User ditemukan");
    }

    puts("Kamu bisa melakukan:");
    puts("1. USE [nama_database];");
    puts("2. CREATE DATABASE [nama_database];");
    puts("3. CREATE TABLE [nama_tabel] ([nama_kolom] [tipe_data], ...);");
    puts("4. DROP DATABASE [nama_database];");
    puts("5. DROP TABLE [nama_table]");
    puts("6. DROP COLUMN [nama-kolom] FROM [Nama_tabel]");
    puts("7. INSERT INTO [nama_tabel] ([value], ...);");
    puts("8. UPDATE [nama_tabel] SET [kolom]=[nilai];");
    puts("9. UPDATE [nama_tabel] SET [kolom]=[nilai] WHERE [kolom]=[value];");
    puts("10. DELETE FROM [nama_tabel];");
    puts("11. DELETE FROM [nama_tabel] WHERE [kolom]=[value];");
    puts("12. SELECT * FROM [nama_tabel];");
    puts("13. SELECT [Nama_kolom] FROM [nama_tabel];");
    puts("14. SELECT * FROM [nama_tabel] WHERE [kolom]=[value];");
    puts("15. SELECT [nama_kolom] FROM [nama_tabel] WHERE [kolom]=[value];");
    puts("16. EXIT");


    puts("Masukkan command yang kamu inginkan:");

    while (1) {
        fgets(command, sizeof(command), stdin);

        //cek apakah user selain root mengirimkan perintah "CREATE USER"
        if (strstr(command, "CREATE USER") != NULL) {
            printf("Anda hanya dapat mengirim perintah CREATE USER sebagai root.\n");
            continue;
        } 
        
        //cek apakah user selain root mengirimkan perintah "GRANT PERMISSION"
        else if(strstr(command, "GRANT PERMISSION") != NULL){
            puts("Anda hanya dapat mengirim perintah GRANT PERMISSION sebagai root.");
            continue;
        }

        //cek apakah user selain root mengirimkan perintah "EXIT"
        else if(strstr(command, "EXIT") != NULL){

            sprintf(full_command, "\"%s\" %s", user, command);
            printf("Full command: %s\n", full_command);
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Gagal mengirim data pengguna ke server");
                exit(EXIT_FAILURE);
            }

            return 0;
        }
        
        //menghandle perintah user selain root
        else {

            //menambahkan user + command
            //contoh: "jisoo" CREATE DATABASE db1;
            sprintf(full_command, "\"%s\" %s", user, command);
            printf("Full command: %s\n", full_command);
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Gagal mengirim data pengguna ke server");
                exit(EXIT_FAILURE);
            }

            //menerima respon dari server
            char server_response[2000];
            if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
                perror("Gagal menerima respons dari server");
                exit(EXIT_FAILURE);
            }

            printf("Respon dari server: %s\n", server_response);
            continue;  // Membaca input lagi
        }
    }
}
```
- **char user[2000], password[2000]; int option** : Mendeklarasikan variabel user dan password untuk menyimpan username dan password, serta variabel option untuk digunakan dalam switch case.

- Blok **while ((option = getopt(argc, argv, "u:p:")) != -1)** { ... }: Loop ini menggunakan fungsi getopt untuk parsing argument opsi command-line. Argumen **'u'** dan **'p'** mencocokkan username dan password, yang disalin ke variabel user dan password.

- **FILE* file = fopen("users.txt", "r"); int userFound = 0** : Membuka file "users.txt" untuk dibaca, yang seharusnya berisi daftar pengguna yang valid. Juga mendeklarasikan flag userFound untuk menunjukkan apakah user valid telah ditemukan.

- **if (file == NULL)** { ... } **else** { ... }: Bagian ini memeriksa apakah file dapat dibuka. Jika tidak, itu berarti file belum ada, jadi itu mencoba membuat file tersebut dan kemudian membukanya untuk membaca. Kemudian, apakah itu harus membuat file atau file sudah ada, bagian ini membaca setiap baris dari file dan memeriksa apakah username dan password ada dalam baris tersebut. Jika ditemukan, userFound diatur ke 1 dan pembacaan berhenti.

- **if (userFound == 0)** { ... } **else** { ... }: Jika pengguna tidak ditemukan dalam file, program mencetak pesan kesalahan dan keluar. Jika pengguna ditemukan, program mencetak pesan konfirmasi dan melanjutkan.

- **puts("Kamu bisa melakukan:")**; ... **puts("16. EXIT")** : Menunjukkan opsi perintah yang tersedia untuk pengguna non-root.

- Bagian **while (1)** { ... }: Ini adalah loop utama yang membaca perintah dari pengguna, memeriksa apakah itu adalah salah satu perintah khusus ("CREATE USER", "GRANT PERMISSION", atau "EXIT"), dan kemudian mengirim perintah tersebut ke server atau memberi pesan kesalahan jika perintah tersebut tidak diperbolehkan untuk pengguna non-root. Jika perintah "EXIT" dikirim, program selesai; jika tidak, loop berlanjut dan membaca perintah lain.

### Selain root, user yang lain tidak bisa melakukan perintah GRANT PERMISSION
```
//cek apakah user selain root mengirimkan perintah "GRANT PERMISSION"
else if(strstr(command, "GRANT PERMISSION") != NULL){
    puts("Anda hanya dapat mengirim perintah GRANT PERMISSION sebagai root.");
    continue;
}
```
- **else if(strstr(command, "GRANT PERMISSION") != NULL)**: Fungsi strstr() mencari kemunculan string "GRANT PERMISSION" dalam perintah yang dimasukkan oleh pengguna. Jika string tersebut ditemukan, fungsi ini akan mengembalikan pointer ke awal kemunculan string tersebut dalam perintah, jika tidak, fungsi ini akan mengembalikan NULL.

- **puts("Anda hanya dapat mengirim perintah GRANT PERMISSION sebagai root.")** : Ini akan menampilkan pesan ke pengguna bahwa hanya root yang bisa menjalankan perintah "GRANT PERMISSION".

- **continue** : Perintah ini mengakibatkan program untuk melanjutkan ke iterasi berikutnya dari loop saat ini, dalam hal ini loop while. Artinya, kode setelah perintah continue ini dalam blok loop saat ini tidak akan dijalankan untuk iterasi saat ini.


## Database
```
int main() {
    char use_database[100] = {0};


    system("mkdir -p databases");


    system("touch permission.txt");
    system("sudo chmod 777 permission.txt");


    system("touch use_database.txt");
    system("sudo chmod 777 use_database.txt");
    // ...
}
```
Pertama-tama pada fungsi ``main()`` akan dibuat array bernama ``use_database`` dengan tipe karakter yang diinisialisasi dengan nilai 0. Variabel ini digunakan untuk menyimpan database yang sedang dikerjakan oleh user. Kemudian, menggunakan fungsi ``system()``, program akan membuat folder ``databases`` menggunakan perintah shell ``mkdir -p databases``. Selanjutnya, file ``permission.txt`` dan ``use_database.txt`` dibuat menggunakan perintah ``touch``, dan hak akses file tersebut diubah menggunakan ``sudo chmod 777`` agar dapat ditulis dan dibaca oleh semua pengguna.


```
    // ...
    int server_fd, new_socket[1000], valread;
    struct sockaddr_in address;
    int addrlen = sizeof(address);


    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Gagal membuat socket");
        exit(EXIT_FAILURE);
    }


    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(SERVER_PORT);


    if (bind(server_fd, (struct sockaddr*)&address, sizeof(address)) < 0) {
        perror("Gagal melakukan binding");
        exit(EXIT_FAILURE);
    }


    if (listen(server_fd, 3) < 0) {
        perror("Gagal mendengarkan koneksi");
        exit(EXIT_FAILURE);
    }
    // ...
```
Lalu, socket server dibuat dengan menggunakan fungsi ``socket()`` dengan domain ``AF_INET`` dan tipe ``SOCK_STREAM``. Jika gagal dalam pembuatan socket, pesan error akan dicetak ke layar. Selanjutnya, struktur address diatur dengan informasi tentang alamat server, seperti alamat IP dan port. Setelah itu, menggunakan fungsi ``bind()``, socket server akan diikat ke alamat yang ditentukan. Jika proses binding gagal, pesan error akan dicetak ke layar. Selanjutnya, menggunakan fungsi ``listen()``, server akan mulai mendengarkan koneksi dari client. Jika proses listen gagal, pesan error akan dicetak ke layar.


```
create_daemon();
```
Setelah itu, fungsi create_daemon() akan dipanggil untuk membuat program berjalan sebagai daemon.


```
void create_daemon(){


    pid_t pid, sid;  


    pid = fork();  


    if (pid < 0) {
        exit(EXIT_FAILURE);
    }


    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }
    // ...
}
```
Fungsi ``fork()`` digunakan untuk menduplikasi proses saat ini. Jika nilai pid yang dikembalikan oleh ``fork()`` adalah negatif, maka proses duplicating gagal dan program akan keluar dengan status kesalahan. Jika nilai pid adalah positif, ini menandakan bahwa kita berada dalam proses induk (parent process), sehingga program akan keluar dengan status keberhasilan. Dalam hal ini, proses asli (parent process) akan segera keluar, sementara proses duplikat (child process) akan melanjutkan eksekusi.


```
    // ...
    umask(0);


    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
    // ...
```
Pada bagian ini, kita mengatur umask ke nilai 0 untuk memastikan bahwa daemon dapat memiliki akses penuh terhadap file yang dibuatnya. Kemudian, menggunakan fungsi ``setsid()``, daemon akan menciptakan session ID baru. Jika nilai sid kurang dari 0, ini menandakan bahwa gagal dalam membuat session ID dan program akan keluar dengan status kesalahan.


```
    // ...
    if ((chdir("/mnt/d/anneu/smt4/sisop/finalPraktikum")) < 0) {
        exit(EXIT_FAILURE);
    }
}
```
Setelah itu direktori kerja program diubah menjadi "/mnt/d/anneu/smt4/sisop" menggunakan fungsi ``chdir()``. Jika perubahan direktori gagal, program akan keluar dengan status kesalahan. Dengan ini, fungsi ``create_daemon()`` berhasil menciptakan sebuah daemon yang siap untuk berjalan dalam background.


```
    // ...
    while (1) {
        if ((new_socket[jumlah_client] = accept(server_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen)) < 0) {
            perror("Gagal menerima koneksi");
            exit(EXIT_FAILURE);
        }


        pthread_create(&(tid[jumlah_client]), NULL, start, &new_socket[jumlah_client]);


        jumlah_client++;


        printf("Client ke-%d terhubung\n", jumlah_client);
    }


    return 0;
}
```
Selanjutnya, dilakukan pengulangan ``while`` yang akan terus berjalan secara tak terbatas. Di dalam loop ini, server menerima koneksi dari client menggunakan fungsi ``accept()``. Jika koneksi diterima, sebuah thread baru akan dibuat menggunakan fungsi ``pthread_create()`` untuk menangani koneksi tersebut. Variabel ``tid`` merupakan array yang menyimpan ID thread untuk setiap client. Setiap kali koneksi diterima, variabel ``jumlah_client`` akan diincrement dan pesan "Client ke-[nomor client] terhubung" akan dicetak ke layar.


Fungsi ``pthread_create()`` akan membuat thread baru dan mengeksekusi fungsi ``start``. Fungsi ``start`` akan dieksekusi sebagai thread terpisah untuk menangani koneksi dengan client yang baru saja terhubung.
```
void* start(void *arg){


    int new_socket = *(int *) arg;
    char buffer[10000];
    // ...
```
Fungsi ini dimulai dengan mendeklarasikan variabel ``new_socket`` yang akan digunakan untuk menampung nilai dari argumen yang diterima. Nilai tersebut diambil dengan menggunakan casting dari ``void*`` menjadi ``int*``. Kemudian, sebuah array karakter ``buffer`` juga dideklarasikan dengan ukuran 10000, yang akan digunakan untuk menyimpan data yang diterima dari client.


```
while (1) {
    int bytes_received = recv(new_socket, buffer, sizeof(buffer) - 1, 0);
    if (bytes_received < 0) {
        perror("Gagal menerima data dari client");
        exit(EXIT_FAILURE);
    }
    buffer[bytes_received] = '\0'; // Null-terminate the received data


    printf("Command: %s\n", buffer);


    send(new_socket, buffer, strlen(buffer), 0);
    // ...
}
```
Loop tak terbatas ``while (1)`` akan terus menerima informasi dari client. Menggunakan fungsi ``recv()``, data diterima dari socket ``new_socket`` dan disimpan dalam array ``buffer``. Jika terjadi kesalahan dalam menerima data, pesan kesalahan akan dicetak dan program akan keluar dengan ``exit(EXIT_FAILURE)``. Setelah itu, karakter null ``(\0)`` ditambahkan di akhir buffer untuk menandakan akhir data yang diterima. Selanjutnya, pesan "Command: [isi buffer]" akan dicetak ke layar untuk menampilkan perintah yang diterima dari client. Kemudian, menggunakan fungsi ``send()``, pesan yang sama akan dikirim kembali ke client sebagai konfirmasi bahwa perintah telah diterima.


```
char *player = strtok(buffer, "\"");
printf("player: %s\n", player);


char *cmnd = strtok(NULL, ";");
cmnd += strspn(cmnd, " ");
printf("command: %s\n", cmnd);
```
Lalu, akan digunakan fungsi ``strtok()`` untuk memisahkan string dalam buffer berdasarkan tanda kutip ganda (") dan titik koma (;). Pemisahan dilakukan dengan tujuan untuk mendapatkan nama pengguna (player) dan perintah (cmnd) yang terdapat dalam buffer. Setelah pemisahan, fungsi ``strspn()`` digunakan untuk menghapus spasi yang mungkin ada di awal perintah (cmnd).


```
log_command(player, cmnd);


char detect[10000], argument[10000];
sscanf(cmnd, "%s %s", detect, argument);
```
Selanjutnya fungsi ``log_command(player, cmnd)`` digunakan untuk mencatat perintah yang diterima dalam log. Kemudian, menggunakan fungsi ``sscanf(cmnd, "%s %s", detect, argument)``, kata pertama dan kata kedua dari string cmnd dipisahkan dan disimpan dalam variabel detect dan argument secara berurutan.


Dalam fungsi ``log_command``, akan dicatat perintah yang diterima dalam file log dengan format yang ditentukan.
```
void log_command(const char *username, const char *command) {
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);
    // ...
```
Dimulai dengan mengambil waktu saat ini menggunakan fungsi ``time()`` dan mengkonversinya ke dalam struktur struct tm menggunakan fungsi ``localtime()``. Timestamp yang telah diformat sesuai dengan format yang diinginkan (YYYY-MM-DD HH:MM:SS) disimpan dalam array ``timestamp``.


```
FILE *file = fopen("log.txt", "a");
if (file == NULL) {
    perror("Gagal membuka file log.txt");
return;
}
```
Selanjutnya, file log dengan nama ``log.txt`` dibuka dalam mode append ``"a"`` menggunakan ``fopen()``. Jika gagal membuka file, pesan kesalahan akan dicetak dan fungsi akan dihentikan.


```
char cleaned_command[100];
strcpy(cleaned_command, command);
char *semicolon_pos = strchr(cleaned_command, ';');
if (semicolon_pos != NULL) {
    *semicolon_pos = '\0';
}
```
Kemudian, perintah yang diterima akan disalin ke dalam array ``cleaned_command``. Jika terdapat karakter semikolon (;) dalam perintah, karakter tersebut akan dihapus dengan menempatkan karakter null ``\0`` pada posisi semikolon tersebut.


```
fprintf(file, "%s:%s:%s\n", timestamp, username, cleaned_command);


fclose(file);
```
Selanjutnya, perintah yang telah diformat dengan timestamp, username, dan cleaned_command dicetak ke file log menggunakan fprintf().Dan terakhir, file log ditutup dengan fclose().


Kembali ke dalam fungsi ``start()``, akan dilakukan serangkaian kondisional (if-else if) yang akan mengecek perintah yang diterima dan menjalankan fungsi yang sesuai tergantung dari perintah tersebut.
```
#define SERVER_PORT 8888
#define MAX_COMMAND_LENGTH 2000
char server_message[MAX_COMMAND_LENGTH];


pthread_t tid[1000];


const char *databasesPath = "/mnt/d/anneu/smt4/sisop/finalPraktikum/database/databases";


// Function to send data over the socket
int send_all(int sockfd, const char* data, int length) {
    int total_sent = 0;
    int bytes_left = length;
    int bytes_sent;


    while (total_sent < length) {
        bytes_sent = send(sockfd, data + total_sent, bytes_left, 0);
        if (bytes_sent == -1) {
            return -1; // Error occurred
        }
        total_sent += bytes_sent;
        bytes_left -= bytes_sent;
    }


    return 0; // Success
}


//cek permission suatu user dalam database
int cek_permission(const char* database, const char* user){


    FILE* file = fopen("permission.txt", "r");
    if (file == NULL) {
        perror("Gagal membuka file permission.txt");
        exit(EXIT_FAILURE);
    }


    char line[100];
    int aksesFound = 0;
    while (fgets(line, sizeof(line), file)) {
        line[strcspn(line, "\n")] = '\0';
        if (strstr(line, database) != NULL) {
            if (strstr(line, user) != NULL) {
                aksesFound = 1;
                break;
            }
        }
    }


    fclose(file);


    if(aksesFound == 1) return 1;
    else return 0;
}


//cek apakah database telah tersedia. Hal tersebut tercatat dalam permission.txt karena suatu database pasti memiliki setidaknya 1 user yang dapat mengaksesnya
int cek_database(const char* database){


    FILE* file = fopen("permission.txt", "r");
    if (file == NULL) {
        perror("Gagal membuka file permission.txt");
        exit(EXIT_FAILURE);
    }


    char line[100];
    int databaseFound = 0;
    while (fgets(line, sizeof(line), file)) {
        line[strcspn(line, "\n")] = '\0';
        if (strstr(line, database) != NULL) {
            databaseFound = 1;
            break;
        }
    }


    fclose(file);


    if(databaseFound == 1) return 1;
    else return 0;
}


//cek apakah user telah terbuat oleh root.
int isUserExist(const char* user) {


    FILE* file = fopen("users.txt", "r");
    if (file == NULL) {
        puts("User menjadi user pertama");
        return 0;
    }


    char line[100];
    while (fgets(line, sizeof(line), file) != NULL) {
        char username[50];
        char password[50];
        if (sscanf(line, "%s %s", username, password) == 2) {
            if (strcmp(user, username) == 0) {
                fclose(file);
                return 1; // Pengguna sudah ada
            }
        }
    }


    fclose(file);
    return 0; // Pengguna belum ada
}


//menambahkan hak akses user baru ke suatu database.
void hak_akses(const char* player, const char* database_name){


    FILE* ptr = fopen("permission.txt","a");
    fprintf(ptr, "%s %s\n", database_name, player);
    fclose(ptr);
}


//menambahkan permission database ke suatu user saat mengirimkan perintah "GRANT PERMISSION"
void give_permission(const char* database, const char* user){


    //cek apakah database telah terbuat
    if(cek_database(database) == 1){
        //cek apakah user tersebut ada
        if(isUserExist(user) == 0){
            puts("Pengguna tidak tersedia");
            return;
        }


        else{
            int hasil = cek_permission(database, user);
            //cek apakah user telah memiliki akses database tersebut atau belum
            if(hasil == 0) hak_akses(user, database);
            else puts("User telah memiliki akses");
        }
    }
    else{
        puts("Database tidak tersedia");
        return;
    }


}


//menyimpan user baru
void saveUser(const char* user, const char* password) {
    if (isUserExist(user)) {
        printf("Pengguna '%s' sudah ada\n", user);
        strcpy(server_message, "User telah tersedia");
        return;
    }
    else strcpy(server_message, "User berhasil disimpan di database");


    FILE* file = fopen("users.txt", "a");
    if (file == NULL) {
        perror("Gagal membuka file users.txt");
        return;
    }


    fprintf(file, "%s %s\n", user, password);
    fclose(file);


    printf("Menyimpan user '%s' dengan password '%s' ke dalam database\n", user, password);
}


//membuat database
void createDatabase(char* player, char* database_name) {
    // Tambahkan logika untuk membuat database


    //kita lakukan mkdir karena database merupakan suatu folder
    char command[100], command2[500];
    sprintf(command, "mkdir databases/%s", database_name);


    // Menjalankan perintah menggunakan system()
    int status = system(command);


    if (status == 0) {
        printf("Membuat database '%s'\n", database_name);
        strcpy(server_message, "Database berhasil dibuat");
    }
   
    else {
        printf("Gagal membuat database '%s'\n", database_name);
        strcpy(server_message, "Gagal membuat database");
    }


    //membuat permission dari suatu database baru dengan user yang sedang dijalankan
    hak_akses(player, database_name);


}


//menghilangkan permission dari suatu user saat drop database
void removePermission(const char *filename, const char *databaseName) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("Gagal membuka file: %s\n", filename);
        exit(EXIT_FAILURE);
    }


    //membuat temporary file
    char tempFilename[1000];
    sprintf(tempFilename, "%s.temp", filename);
    FILE *tempFile = fopen(tempFilename, "w");
    if (tempFile == NULL) {
        printf("Gagal membuat file sementara: %s\n", tempFilename);
        fclose(file);
        exit(EXIT_FAILURE);
    }


    char line[1000];
    int found = 0;


    //menyimpan data selain database yang ingin dihapus ke file temporary
    while (fgets(line, sizeof(line), file) != NULL) {
        if (strstr(line, databaseName) != NULL) {
            found = 1;
            continue;
        }
        fputs(line, tempFile);
    }


    fclose(file);
    fclose(tempFile);


    //remove filename
    if (remove(filename) != 0) {
        printf("Gagal menghapus file: %s\n", filename);
        exit(EXIT_FAILURE);
    }


    //rename file temporary ke filename
    if (rename(tempFilename, filename) != 0) {
        printf("Gagal mengganti nama file: %s\n", tempFilename);
        exit(EXIT_FAILURE);
    }


    if (found) {
        printf("Data permission dengan database %s dihapus dari file %s\n", databaseName, filename);
    } else {
        printf("Data permission dengan database %s tidak ditemukan di file %s\n", databaseName, filename);
    }
}


//cek login untuk mengetahui suatu user sedang mengakses database apa. hal tersebut disimpan dalam use_database.txt
void cek_login(const char* user) {
    // Buka file use_database.txt untuk membaca
    FILE* file = fopen("use_database.txt", "r");
    if (file == NULL) {
        printf("Gagal membuka file use_database.txt\n");
        return;
    }


    char line[100];
    char tempFile[] = "temp.txt";


    // Buka file temporary untuk menulis
    FILE* temp = fopen(tempFile, "w");
    if (temp == NULL) {
        printf("Gagal membuat file temporary\n");
        fclose(file);
        return;
    }


    int deleted = 0; // Flag untuk menandakan apakah ada baris yang dihapus


    // Baca baris per baris dari file use_database.txt
    while (fgets(line, sizeof(line), file) != NULL) {
        // Periksa apakah baris mengandung substring <user>
        char* token = strstr(line, user);
        if (token != NULL) {
            // Hapus substring <user> dari baris
            memmove(token, token + strlen(user), strlen(token) - strlen(user) + 1);
            deleted = 1;
        } else {
            // Tulis baris yang tidak berubah ke file temporary
            fputs(line, temp);
        }
    }


    // Tutup file
    fclose(file);
    fclose(temp);


    // Hapus file use_database.txt
    if (remove("use_database.txt") == 0) {
        // Ganti nama file temporary menjadi use_database.txt
        if (rename(tempFile, "use_database.txt") != 0) {
            printf("Gagal mengganti nama file\n");
        }
    } else {
        printf("Gagal menghapus file use_database.txt\n");
    }


    if (deleted) {
        printf("Baris dengan user %s dihapus dari file use_database.txt\n", user);
    } else {
        printf("Tidak ada baris dengan user %s dalam file use_database.txt\n", user);
    }
}


//saat user memanggil perintah USE()
void use(const char* database, const char* user, int client_socket){


    char message[1000];
    if(cek_database(database) == 1){


        if((strcmp(user, "root") == 0) || (cek_permission(database, user) == 1)){
            sprintf(message, "User %s menggunakan database %s\n", user, database);
            printf("User %s menggunakan database %s\n", user, database);
            send_all(client_socket, message, strlen(message));


            //memanggil cek_login() untuk memberitahu bahwa user sedang mengakses suatu database
            cek_login(user);


            FILE* file = fopen("use_database.txt", "a");
            if (file != NULL) {
                // Tulis string "<user> <database>" ke dalam file
                fprintf(file, "%s %s\n", user, database);
                // Tutup file setelah selesai menulis
                fclose(file);
            }
           
            else {
                sprintf(message, "Gagal membuka file use_database.txt\n");
                printf("Gagal membuka file use_database.txt\n");


                send_all(client_socket, message, strlen(message));
            }
        }
        else{
            sprintf(message, "User %s tidak memiliki akses database %s\n", user, database);
            printf("User %s tidak memiliki akses database %s\n", user, database);


            send_all(client_socket, message, strlen(message));
        }
    }


    else {
        sprintf(message, "Database tidak ditemukan\n");
        puts("database tidak ditemukan");
        send_all(client_socket, message, strlen(message));
    }
}


//untuk mengetahui user sedang mengakses database apa, diketahui dari perintah use() yang telah dipanggil sebelumnya
void getDatabaseFromFile(const char* user, const char* filename, char* database) {
    FILE* file = fopen(filename, "r");


    if (file == NULL) {
        printf("Failed to open the file\n");
        return;
    }


    char line[500];
    while (fgets(line, sizeof(line), file)) {
        char tmpUser[100], tmpDatabase[100];
        if (sscanf(line, "%s %s", tmpUser, tmpDatabase) == 2) {
            if (strcmp(tmpUser, user) == 0) {
                strcpy(database, tmpDatabase);
                fclose(file);
                return;
            }
        }
    }


    printf("User not found in the file\n");
    fclose(file);
}


//cek apakah tabel telah terbuat
bool isTableExists(const char* path){
    char filePath[500];


    strcpy(filePath, path);


    FILE* file = fopen(filePath, "r");
    if (file) {
        // File ditemukan
        fclose(file);
        return true;
    } else {
        // File tidak ditemukan
        return false;
    }
}


//cek apakah database telah terbuat
bool isDatabaseExists(const char* database_name){


    DIR* dir = opendir("databases");
    if (!dir) {
        // Direktori database tidak ditemukan
        return false;
    }


    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, database_name) == 0) {
            // Direktori dengan nama yang sesuai ditemukan
            closedir(dir);
            return true;
        }
    }


    // Direktori tidak ditemukan
    closedir(dir);
    return false;
}


//menghapus karakter whitespace di awal string text dan mengembalikan pointer ke string yang telah di trim
char* trim(char *text){
    int index = 0;


    while(text[index] == ' ' || text[index] == '\t'){
        index++;
    }
    char *temp = strchr(text,text[index]);
    return temp;
}


//untuk cek apakah buff tersebut merupakan int ataukah string
int validasi(char* buff){
    char tmp[1000];
    strcpy(tmp, buff);


    char g = '\'';


    if(tmp[0] == g && tmp[strlen(tmp) - 1] == g) return 1;


    for(int i = 0; i < strlen(tmp); i++){
       
        //jika bukan merupakan string atau int
        if(tmp[i] < '0' || tmp[i] > '9'){
            return 0;
        }
    }
    return 2;


}


//fungsi yang dijalankan saat user memanggil perintah insert()
void insert(const char* cmnd, const char* database, const char* tableName){


    char tmp[1000];
    strcpy(tmp, cmnd);


    //mengambil value didalam tanda kurung
    char* token = strtok(tmp, "(");
    printf("token1 = %s\n", token);
    token = strtok(NULL, "(");
    printf("token2 = %s\n", token);
    token = strtok(token, ")");
    printf("token3 = %s\n", token);
    token = strtok(token, ",");
    printf("token4 = %s\n", token);


    char data[100][100];


    int i = 0;


    //mengambil kata dalam koma
    while(token != NULL){
        strcpy(data[i], trim(token));
        i++;
        token = strtok(NULL, ",");
        printf("loop token = %s\n", token);
    }


    FILE *filein, *fileout;


    char open[1000] = {0}, appends[1000] = {0};
    sprintf(open, "databases/%s/struktur_%s.txt", database, tableName);


    printf("open: %s\n", open);
    filein = fopen(open, "r");


    char data_type[100][1000];
    char tmp_type[1000];
    int k = 0, l=0;


    //cek table
    if(filein){
        while(fscanf(filein,"%s",tmp_type) != EOF){
            if (l%2 == 1){
                strcpy(data_type[k],tmp_type);
                printf("Datatype[%d]=%s\n",k,data_type[k]);
                k++;
            }
            l++;
        }
    }
    else{
        puts("Table tidak ada");
        return;
    }


    printf("k = %d, i = %d\n", k, i);


    sprintf(appends, "databases/%s/%s.txt", database, tableName);
    fileout = fopen(appends, "a");


    if(k != i){
        fclose(filein);
        fclose(fileout);


        puts("colom count tidak cocok");
        return;
    }


    //melakukan validasi apakah tipe data sesuai dengan yang diinginkan
    for(int j = 0;j < i; j++){
        int val = validasi(data[j]);
        printf("nilai validasi: %d\n", val);
        if(val == 1 && !strcmp(data_type[j],"string")) ;
        else if (val == 2 && !strcmp(data_type[j],"int")) ;
        else {
            fclose(filein);
            fclose(fileout);
            puts("Invalid input");
            return;
        }  
    }


    for(int j = 0;j< i - 1;j++){


        fprintf(fileout,"%s,", data[j]);
        printf("data: %s\n", data[j]);
    }


    fprintf(fileout,"%s\n", data[i-1]);
    fclose(filein);
    fclose(fileout);
}


void update(char *cmnd, char *database){


    if(!strlen(database)){
        puts("No database used");
        return;
    }


    char temp[1024] = {0};
    strcpy(temp,cmnd);


    char* token = strtok(temp, ";");
    token = strtok(temp," ");
    char input[10][1000];
    int i=0;


    // Tokenisasi kemudian disimpan dalam array input
    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }


    if(strcmp(input[2],"SET")){
        puts("Invalid Syntax");
        return;
    }


    char ColName[1000], val[1000];


    token = strtok(input[3],"=");
    strcpy(ColName,token);
    token = strtok(NULL,"=");
    strcpy(val,token);


    char path[10000],RefFile[10000],n_path[10000];


    sprintf(RefFile,"databases/%s/struktur_%s.txt",database,input[1]);
    FILE *strukturin;
    strukturin = fopen(RefFile,"r");


    i = -1;
    int k = 0;
    char ret[1000],table[1000];


    // Cek Tabel
    if(strukturin){
        while(fscanf(strukturin,"%s %s",table,ret) != EOF){
            if(!strcmp(ColName,table)){
                i = k;
            }
            k++;
        }
        if(i == -1){
            fclose(strukturin);
            puts("Column doesn't exist");
        }
        fclose(strukturin);
    }
    else{
        puts("Table doesn't exist");
    }


    int w = -1;
    char req[1000],temporary[1000],valwhere[1000];


    // Kasus ada klausa where
    if(strlen(input[4])){
        token = strtok(input[5],"=");
        strcpy(req,token);
        token = strtok(NULL,"=");
        strcpy(valwhere,token);
        k=0;
        if(!strcmp(input[4],"WHERE")){
            strukturin = fopen(RefFile,"r");
            while(fscanf(strukturin,"%s %s",temporary,ret) != EOF){
                if(!strcmp(req,temporary)){
                    w = k;
                }
                k++;
            }
            if(w == -1){
                fclose(strukturin);
                puts("Column doesn't exist");
                return;
            }
            fclose(strukturin);
        }
    }


    sprintf(path,"databases/%s/%s.txt",database,input[1]);
    sprintf(n_path,"databases/%s/%s2.txt",database,input[1]);
    FILE *filein,*fileout;
    filein = fopen(path,"r");
    fileout = fopen(n_path,"w");
    char ambil[1000], lama[1000];
   
    // Menulis hasil update
    if(filein){
       
        // Membaca dan mengabaikan update baris pertama (header)
        fprintf(fileout, "%s", fgets(ambil, 1000, filein));
        while(fgets(ambil,1000,filein)){
            strcpy(lama,ambil);
            token = strtok(ambil,",");
            int j = 0;
            char baru[1000];
            strcpy(baru,"");
            int flag = 0;
            while(token!=NULL){
                if(j == w){
                    if(!strcmp(valwhere,token)){
                        flag = 1;
                    }
                }
                if(j==i){
                    strcat(baru,val);
                    strcat(baru,",");
                }
                else{
                    strcat(baru,token);
                    strcat(baru,",");
                }
                token = strtok(NULL,",");
                j++;
            }
            if(flag == 1 || w == -1){
                baru[strlen(baru)-1] = 0;
                fprintf(fileout,"%s",baru);
                if(baru[strlen(baru)-1] != '\n'){
                    fprintf(fileout,"\n");
                }
            }
            else{
                lama[strlen(baru)-1] = 0;
                fprintf(fileout,"%s",lama);
                if(lama[strlen(lama)-1] != '\n'){
                    fprintf(fileout,"\n");
                }
            }
        }
        fclose(filein);
        fclose(fileout);
        remove(path);
        rename(n_path,path);
        return;
    }
}




void delete_from(char *cmnd, char *database) {
    if(!strlen(database)){
        puts("No database used");
        return;
    }


    char temp[1024] = {0};
    strcpy(temp, cmnd);


    // Tokenisasi input untuk mendapat nama tabel
    char* token = strtok(temp, ";");
    token = strtok(temp, " ");
    char input[10][1000];
    int i = 0;


    while (token != NULL) {
        strcpy(input[i++], token);
        token = strtok(NULL, " ");
    }


    // Delete semua baris dari tabel kecuali baris 1 header
    if (i == 3) {
        char path[10000];
        sprintf(path, "databases/%s/%s.txt", database, input[2]);
        FILE *filein, *fileout;
        filein = fopen(path, "r");
        if (filein) {
            char ambil[1000];
            fgets(ambil, 1000, filein);
            fclose(filein);


            fileout = fopen(path, "w");
            fprintf(fileout, "%s\n", ambil);
            fclose(fileout);
        }
        else {
            puts("Table doesn't exist");
            return;
        }
        return;
    }


    // Kasus jika terdapat klausa where
    else if (i == 5) {
        if (strcmp(input[3], "WHERE")) {
            puts("Invalid Syntax");
            return;
        }


        char TableName[1000], val[1000];


        // Tokenisasi untuk mendapat value where
        token = strtok(input[4], "=");
        strcpy(TableName, token);
        token = strtok(NULL, "=");
        strcpy(val, token);


        char path[10000], RefFile[10000], n_path[10000];


        sprintf(RefFile, "databases/%s/struktur_%s.txt", database, input[2]);
        FILE *strukturin;
        strukturin = fopen(RefFile, "r");


        int columnIndex = -1;
        int k = 0;
        char ret[1000], table[1000];
        if (strukturin) {
            while (fscanf(strukturin, "%s %s", table, ret) != EOF) {
                if (!strcmp(TableName, table)) {
                    columnIndex = k;
                }
                k++;
            }
            if (columnIndex == -1) {
                fclose(strukturin);
                puts("Column doesn't exist");
                return;
            }
            fclose(strukturin);
        }
        else {
            puts("Table doesn't exist");
            return;
        }


        sprintf(path, "databases/%s/%s.txt", database, input[2]);
        sprintf(n_path, "databases/%s/%s2.txt", database, input[2]);
        FILE *filein, *fileout;
        filein = fopen(path, "r");
        fileout = fopen(n_path, "w");
        char ambil[1000];
       
        // Mengganti struktur tabel dengan memindahkan file lama yang sesuai ke file baru kemudian direname
        if (filein) {
            int lineCount = 0;
            while (fgets(ambil, 1000, filein)) {
                lineCount++;
                if (lineCount == 1) {
                    fprintf(fileout, "%s", ambil);  // Menyalin baris pertama ke fileout
                    continue;
                }
                token = strtok(ambil, ",");
                int j = 0;
                char baru[1000];
                strcpy(baru, "");
                int flag = 0;
                while (token != NULL) {
                    strcat(baru, token);
                    strcat(baru, ",");
                    if (j == columnIndex) {
                        if (!strcmp(val, token)) {
                            flag = 1;
                        }
                    }
                    token = strtok(NULL, ",");
                    j++;
                }
                if (!flag) {
                    baru[strlen(baru)-1] = 0;
                    fprintf(fileout, "%s", baru);
                    if (baru[strlen(baru)-1] != '\n') {
                        fprintf(fileout, "\n");
                    }
                }
            }
            fclose(filein);
            fclose(fileout);
            remove(path);
            rename(n_path, path);
            return;
        }
    }
}




void select_table(char *cmnd, char *database) {
    if (!strlen(database)) {
        puts("no database used");
        return;
    }


    printf("database: %s\n", database);


    char temp[1024] = {0};
    strcpy(temp, cmnd);


    char* token = strtok(temp, ";");
    token = strtok(temp, " ");
    char input[10][1000];
    int i = 0;


    // Menyimpan potongan command kedalam array input
    while (token != NULL) {
        strcpy(input[i++], token);
        token = strtok(NULL, " ");
    }


    printf("Input values:\n");
    for (int j = 0; j < i; j++) {
        printf("input[%d]: %s\n", j, input[j]);
    }


    int all, w = -1, k, kasus, urut[20];
    char banding[1000], temporary[1000], cmpwhere[1000], ret[1000];
    char path[10000], struk[10000], n_path[10000], cmp[20][1000];


    FILE *strukturin;


    // Untuk mendapatkan select "*"
    if (!strcmp(input[1], "*")) {
        sprintf(struk, "databases/%s/struktur_%s.txt", database, input[3]);


        strukturin = fopen(struk, "r");


        if (!strukturin) {
            puts("table does not exist");
            return;
        }


        fclose(strukturin);


        all = -1;
        kasus = 2;
        if (strcmp(input[2], "FROM")) {
            puts("invalid syntax");
            return;
        }
        // Cek klausa where
        if (i >= 5 && !strcmp(input[4], "WHERE")) {
            token = strtok(input[5], "=");
            strcpy(banding, token);
            token = strtok(NULL, "=");
            strcpy(cmpwhere, token);


            k = 0;


            if (!strcmp(input[4], "WHERE")) {
                strukturin = fopen(struk, "r");
                while (fscanf(strukturin, "%s %s", temporary, ret) != EOF) {
                    if (!strcmp(banding, temporary)) {
                        w = k;
                    }
                    k++;
                }
                if (w == -1) {
                    fclose(strukturin);
                    puts("column does not exist");
                    return;
                }
                fclose(strukturin);
            }
        }
    }
    // Untuk mendapatkan select beberapa parameter kolom
    else {
        kasus = 2;
        while (kasus <= i && strcmp(input[kasus++], "FROM")) {}


        if (kasus > i) {
            puts("invalid syntax");
            return;
        }
        sprintf(struk, "databases/%s/struktur_%s.txt", database, input[kasus]);
        strukturin = fopen(struk, "r");


        if (!strukturin) {
            puts("table does not exist");
            return;
        }


        fclose(strukturin);


        all = 0;
        kasus = 1;


        for (int oo = 0; oo < 20; oo++) {
            urut[oo] = -1;
        }


        char temp2[1000];
        while (kasus < i && strcmp(input[kasus], "FROM")) {
            k = 0;
            strukturin = fopen(struk, "r");
            while (fscanf(strukturin, "%s %s", temporary, ret) != EOF) {
                strcpy(temp2, temporary);
                strcat(temp2, ",");
                if (!strcmp(input[kasus], temporary) || !strcmp(temp2, input[kasus])) {
                    strcpy(cmp[all], input[kasus]);
                    urut[all] = k;
                    all++;
                }
                k++;
            }
            if (all == 0 || urut[all - 1] == -1) {
                fclose(strukturin);
                return;
            }
            fclose(strukturin);
            kasus++;
        }
        if (kasus == i) {
            return;
        }
        if (i >= kasus + 2 && !strcmp(input[kasus + 2], "WHERE")) {
            token = strtok(input[kasus + 3], "=");
            strcpy(banding, token);
            token = strtok(NULL, "=");
            strcpy(cmpwhere, token);


            k = 0;


            if (!strcmp(input[kasus + 2], "WHERE")) {
                strukturin = fopen(struk, "r");
                while (fscanf(strukturin, "%s %s", temporary, ret) != EOF) {
                    if (!strcmp(banding, temporary)) {
                        w = k;
                    }
                    k++;
                }
                if (w == -1) {
                    fclose(strukturin);
                    return;
                }
                fclose(strukturin);
            }
        }
    }


    sprintf(path, "databases/%s/%s.txt", database, input[kasus + 1]);
    sprintf(n_path, "Hasil_select.txt");


    printf("path: %s\n", path);
    printf("n_path: %s\n", n_path);


    // Attempt to create the output file using touch
    FILE *touch_file = fopen(n_path, "a");
    if (touch_file) {
        fclose(touch_file);
        puts("Output file created.");
    }


    FILE *filein, *fileout;
    filein = fopen(path, "r");
   
    // Menulis hasil select di hasil_select.txt
    if (filein) {


        // Check if output file exists
        fileout = fopen(n_path, "r");
        if (fileout) {
            // Output file already exists
            fclose(fileout);
            puts("Output file already exists.");
        } else {
            // Output file doesn't exist, create it
            fileout = fopen(n_path, "w");
            if (fileout) {
                fclose(fileout);
                puts("Output file created.");
            } else {
                puts("Failed to create output file.");
                return;
            }
        }
        // Open the output file for writing
        fileout = fopen(n_path, "w");
        if (!fileout) {
            puts("Failed to open output file for writing.");
            return;
        }


        char ambil[1000], lama[1000], baru[1000];


        while (fgets(ambil, 1000, filein)) {
            if (all == -1) {
                if (w == -1) {
                    fprintf(fileout, "%s", ambil);
                }
                else {
                    char hade[1000];
                    strcpy(hade, ambil);
                    token = strtok(ambil, ",");
                    int j = 0;
                    strcpy(baru, "");
                    int flag = 0;
                    while (token != NULL) {
                        if (j == w) {
                            if (!strcmp(cmpwhere, token)) {
                                fprintf(fileout, "%s", hade);
                                break;
                            }
                        }
                        token = strtok(NULL, ",");
                        j++;
                    }
                }
            }
            else {
                char jadi[1000], hade[1000];
                strcpy(jadi, "");
                for (int oo = 0; oo < all; oo++) {
                    strcpy(hade, ambil);
                    token = strtok(hade, ",");
                    int j = 0;
                    strcpy(baru, "");
                    int flag = 0;
                    while (token != NULL) {
                        if (j == urut[oo]) {
                            strcat(jadi, token);
                            strcat(jadi, ",");
                            break;
                        }
                        token = strtok(NULL, ",");
                        j++;
                    }
                }
                jadi[strlen(jadi) - 1] = '\0';
                fprintf(fileout, "%s\n", jadi);
            }
        }


        fclose(filein);
        fclose(fileout);
        puts("Select result has been written to the file 'database/Hasil_select.txt'");
    }
    else {
        puts("Failed to open input or output file");
    }
}




void handleDump(char *db_name, char *dumpOutput) {
    //cek apakah ada database
    if (strlen(db_name) == 0) {
        puts("No database used");
        return;
    }


    //buat file path
    char filePath[100];
    snprintf(filePath, sizeof(filePath), "databases/%s", db_name);


    DIR *dir = opendir(filePath);
    if (dir == NULL) {
        perror("Error opening directory");
        printf("Directory path: %s\n", filePath);
        return;
    }


    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strncmp(entry->d_name, "struktur_", 9) != 0) {
            // Create the file path
            char file[2000];
            snprintf(file, sizeof(file), "%s/%s", filePath, entry->d_name);


            // Open the file
            FILE *filePtr = fopen(file, "r");
            if (filePtr == NULL) {
                printf("Error opening file: %s\n", file);
                continue;
            }


            // Read the first line
            char firstLine[1000];
            if (fgets(firstLine, sizeof(firstLine), filePtr) != NULL) {
                // Remove newline character from the end of the line
                if (firstLine[strlen(firstLine) - 1] == '\n') {
                    firstLine[strlen(firstLine) - 1] = '\0';
                }


                // Remove file extension
                char *extension = strrchr(entry->d_name, '.');
                if (extension != NULL) {
                    *extension = '\0';
                }


                // Append the output to the dumpOutput variable
                sprintf(dumpOutput + strlen(dumpOutput), "DROP TABLE %s;\n", entry->d_name);
                sprintf(dumpOutput + strlen(dumpOutput), "CREATE TABLE %s (%s);\n", entry->d_name, firstLine);


                // Read the rest of the lines
                char line[1000];
                while (fgets(line, sizeof(line), filePtr) != NULL) {
                    // Remove newline character from the end of the line
                    if (line[strlen(line) - 1] == '\n') {
                        line[strlen(line) - 1] = '\0';
                    }


                    // Append the output to the dumpOutput variable
                    sprintf(dumpOutput + strlen(dumpOutput), "INSERT INTO %s (%s);\n", entry->d_name, line);
                }


                sprintf(dumpOutput + strlen(dumpOutput), "\n");
            }


            // Close the file
            fclose(filePtr);
        }
    }


    closedir(dir);
}




int main(){
        //...


        //jika kata awalnya merupakan CREATE
        if(strcmp(detect, "CREATE") == 0){
            //jika kata selanjutnya merupakan USER
            if(strcmp(argument, "USER") == 0){


                char user[100];
                char password[100];


                //mengambil value user dan password dari terminal
                sscanf(cmnd, "CREATE USER %s IDENTIFIED BY %s", user, password);
                //menjalankan fungsi saveUser()
                saveUser(user, password);
            }
            //jika kata selanjutnya merupakan DATABASE
            else if(strcmp(argument, "DATABASE") == 0){
                char database_name[100];
                //mengambil value database_name
                sscanf(cmnd, "CREATE DATABASE %s", database_name);
                //menjalankan fungsi createDatabase()
                createDatabase(player, database_name);
            }
            //jika kata selanjutnya merupakan TABLE
            else if(strcmp(argument, "TABLE") == 0){
               
                //mengetahui database yang sedang digunakan user() dari use_database.txt
                char database[500];
                getDatabaseFromFile(player, "use_database.txt", database);


                printf("database: %s\n", database);


                char tableName[1000];
                char columns[1000];


                //jika user telah menggunakan database, setelah akses USE()
                if (database[0] != '\0'){
                   
                    //mengambil value tableName dan juga isinya
                    if (sscanf(cmnd, "CREATE TABLE %s (%[^)])", tableName, columns) == 2) {
                        printf("Table name: %s\n", tableName);
                        printf("Columns: %s\n", columns);


                        //kita cek dulu apakah user memiliki akses di suatu database
                        int hasil = cek_permission(database, player);


                        if(hasil == 0) puts("User tidak memiliki akses");
                        else{


                            // Membuat nama file dengan format <tableName>.txt
                            char filename[5000], filename2[5000];
                            char tmp[10000], tmp2[10000];
                            sprintf(filename, "databases/%s/%s.txt", database, tableName);
                            sprintf(filename2, "databases/%s/struktur_%s.txt", database, tableName);
                           
                            //cek apakah tabel telah tersedia
                            if(isTableExists(filename) == 1) puts("Table is already existed");
                            else{
                                //jika belum tersedia maka buat terlebih dahulu dan ubah permission jadi 777
                                sprintf(tmp, "touch %s", filename);
                                sprintf(tmp2, "sudo chmod 777 %s", filename);


                                system(tmp);
                                system(tmp2);


                                sprintf(tmp, "touch %s", filename2);
                                sprintf(tmp2, "sudo chmod 777 %s", filename2);
                                system(tmp);
                                system(tmp2);
                               
                                // Membuka file untuk ditulis
                                FILE *file = fopen(filename, "w");
                                if (file == NULL) {
                                    printf("Failed to create file\n");
                                }
                               
                                else {
                                    // Menulis isi kolom ke dalam file
                                    fprintf(file, "%s\n", columns);
                                    fclose(file);
                                    printf("Columns have been saved to file %s\n", filename);
                                }


                                FILE *file2 = fopen(filename2, "w");
                                if(file2 == NULL) puts("Failed to create file");
                                else{


                                    char* token;
                                    token = strtok(columns, ",");


                                    int i = 0;


                                    while(token != NULL){


                                            while (*token == ' ') token++;
                                            if (strlen(token) > 0) {
                                                fprintf(file2, "%s\n", token);
                                                printf("%s\n", token);
                                            }
                                           
                                            // Menggunakan strtok lagi untuk memperoleh token berikutnya
                                            token = strtok(NULL, ",");
                                       
                                        i++;
                                    }


                                    fclose(file2);
                                }
                            }


                        }


                    }
                   
                    else {
                        puts("User belum menggunakan database apapun. Gunakan command USE terlebih dahulu");
                    }
                }


                else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");


           
            }
        }
        //Jika kata awalnya merupakan GRANT
        else if(strcmp(detect, "GRANT") == 0){


            char nama_database[100];
            char user_database[100];
            //mengambil value nama_database dan user_database
            sscanf(cmnd, "GRANT PERMISSION %s INTO %s", nama_database, user_database);
           
            //menjalankan fungsi give_permission() untuk memberikan akses user agar dapat mengakses database yang diinginkan
            give_permission(nama_database, user_database);
        }


        //Jika kata awalnya merupakan USE
        else if(strcmp(detect, "USE") == 0){


            char databaseName[100];


            //mengambil value databaseName
            sscanf(cmnd, "USE %s", databaseName);
            //menjalankan fungsi use() untuk mengetahui bahwa player akan akses suatu database
            use(databaseName, player, new_socket);
        }


        //jika kata awalnya merupakan DROP
        else if(strcmp(detect, "DROP") == 0){
            //Jika kata berikutnya merupakan DATABASE
            if(strcmp(argument, "DATABASE") == 0){
               
                //mengambil value databaseName
                char databaseName[5000];
                sscanf(cmnd, "DROP DATABASE %s", databaseName);
                //cek permission dari user ke suatu database. jika dia merupakan root maka bebas
                if(cek_permission(databaseName, player) || (strcmp(player, "root") == 0)){
                    //cek apakah database tersedia
                    if(isDatabaseExists(databaseName)){
                        char cmd[10000];


                        //jika tersedia maka hapus database
                        sprintf(cmd, "rm -r databases/%s",databaseName);
                        system(cmd);
                        //menjalankan fungsi removePermission() untuk menghapus user dan database yang tersedia dalam permission.txt
                        removePermission("permission.txt", databaseName);
                    }


                    else puts("Database tidak tersedia");
                }
                else puts("User tidak memiliki akses");


            }
            // Jika kata berikutnya merupakan TABLE
            else if(strcmp(argument, "TABLE") == 0){
               
                //mengambil value tableName
                char tableName[500];
                sscanf(cmnd, "DROP TABLE %s", tableName);


                //mengecek user tersebut sedang mengakses database apa
                char database[500];
                getDatabaseFromFile(player, "use_database.txt", database);


                printf("database: %s\n", database);


                if (database[0] != '\0'){
                   
                    char filename[5000];
                    sprintf(filename, "databases/%s/%s.txt", database, tableName);


                    char filename2[5000];
                    sprintf(filename2, "databases/%s/struktur_%s.txt", database, tableName);


                    //cek apakah table tersedia
                    if(isTableExists(filename) == 1){
                        char cmd_drop[10000];
                        sprintf(cmd_drop, "rm -r %s", filename);
                        system(cmd_drop);


                        sprintf(cmd_drop, "rm -r %s", filename2);
                        system(cmd_drop);
                    }
                    else puts("Table tidak tersedia");


                }
                else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");


            }
            //Jika kata berikutnya merupakan COLUMN
            else if(strcmp(argument, "COLUMN") == 0){
                char tableName[500];
                char colName[500];
                sscanf(cmnd, "DROP COLUMN %s FROM %s;", colName, tableName);


                char database[500];
                getDatabaseFromFile(player, "use_database.txt", database);


                if(!strlen(database)){
                    puts("no database used");
                }


                char input[10][1000];
                char tmp[10000];
                strcpy(tmp,cmnd);
                int k = 0;
                char *token;
                token = strtok(tmp,";");
                token = strtok(token," ");
                while (token != NULL) {
                    strcpy(input[k++],token);
                    token = strtok(NULL, " ");
                }


                if(k != 5){
                    puts("Invalid Syntax");
                }


                FILE *strukturin,*strukturout
                    ,*tablein,*tableout;


                char open[10000]={0},append[10000]={0},a[1000]={0},b[1000]={0};
                sprintf(open,"databases/%s/struktur_%s",database,input[4]);
                strcpy(a,open);
                strcat(a,"2.txt");
                strcat(open,".txt");


                strukturin = fopen(open,"r");
           
                int i = 0,j = 0;
                char data_type[1000], name[1000];


                if(strukturin){
                    strukturout = fopen(a,"w");
                    while(fscanf(strukturin,"%s %s",name,data_type) != EOF){
                        if(strcmp(input[2],name)){
                            fprintf(strukturout,"%s %s\n",name,data_type);
                        }
                        else i = j;
                        j++;
                    }
                    fclose(strukturin);
                    fclose(strukturout);
                }
                else{
                    puts("Table doesn't exist");
                }


                sprintf(append,"databases/%s/%s",database,input[4]);
                strcpy(b,append);
                strcat(b,"2.txt");
                strcat(append,".txt");


                tablein = fopen(append,"r");
                tableout = fopen(b,"w");


                char ambil[1000];
                while(fgets(ambil,1000,tablein)){
                    token = strtok(ambil,",");
                    int j = 0;
                    char baru[1000];
                    strcpy(baru,"");
                    while(token!=NULL){
                        if(j!=i){
                            strcat(baru,token);
                            strcat(baru,",");
                        }
                        token = strtok(NULL,",");
                        j++;
                    }
                    baru[strlen(baru)-1] = 0;
                    fprintf(tableout,"%s",baru);
                    if(baru[strlen(baru)-1] != '\n'){
                        fprintf(tableout,"\n");
                    }
                }
                fclose(tablein);
                fclose(tableout);
                remove(open);
                rename(a,open);
                remove(append);
                rename(b,append);
            }
        }
        //jika kata awalnya merupakan INSERT
        else if(strcmp(detect, "INSERT") == 0){


            //cek user tersebut sedang akses database apa
            char database[500];
            getDatabaseFromFile(player, "use_database.txt", database);


            printf("database: %s\n", database);


            char tableName[500];
            char val[500];
            if (database[0] != '\0'){
               
                //mengambil value tableName dan isi column
                sscanf(cmnd, "INSERT INTO %s (%[^)])", tableName, val);
                char filename[10000];
                sprintf(filename, "databases/%s/%s.txt", database, tableName);


                //cek apakah table tersedia
                if(isTableExists(filename) == 1){
                    //menjalankan fungsi insert()
                    insert(cmnd, database, tableName);
                }
                else puts("Table tidak tersedia");
            }


            else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");
        }


        else if(strcmp(detect, "UPDATE") == 0){


            char database[500];
            getDatabaseFromFile(player, "use_database.txt", database);


            printf("database : %s\n", database);


            char tableName[500];
            char columnName[500];
            char val[500];
            if (database[0] != '\0'){


                sscanf(cmnd, "UPDATE %s SET %s=%s;", tableName, columnName, val);
                char filename[10000];
                sprintf(filename, "databases/%s/%s.txt", database, tableName);


                if(isTableExists(filename) == 1){


                    update(cmnd, database);
                }
                else puts("Table tidak tersedia");
            }


            else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");
        }


        else if(strcmp(detect, "DELETE") == 0){


            char database[500];
            getDatabaseFromFile(player, "use_database.txt", database);


            printf("database : %s\n", database);


            char tableName[500];
            if (database[0] != '\0'){


                sscanf(cmnd, "DELETE FROM %s;", tableName);
                char filename[10000];
                sprintf(filename, "databases/%s/%s.txt", database, tableName);


                if(isTableExists(filename) == 1){


                    delete_from(cmnd, database);
                }
                else puts("Table tidak tersedia");
            }


            else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");
        }


        else if(strcmp(detect, "SELECT") == 0){


            char database[500];
            getDatabaseFromFile(player, "use_database.txt", database);


            printf("database: %s\n", database);


            char tableName[500];
            char col[500];
            if (database[0] != '\0'){


                sscanf(cmnd, "SELECT %[^FROM] FROM %s", col, tableName);


                char filename[10000];
                sprintf(filename, "databases/%s/%s.txt", database, tableName);


                if(isTableExists(filename) == 1){
                   
                    select_table(cmnd, database);
                }
                else puts("Table tidak tersedia");
            }


            else puts("User belum mengakses database apapun. Gunakan USE untuk akses suatu database");
        }


        //apabila exit maka hapus riwayat di cek_login()
        else if(strcmp(detect, "EXIT") == 0){


            cek_login(player);


            return 0;
        }


        //jika perintahnya DUMP maka akan dipanggil fungsi handleDump dengan mengambil nama database dari getDatabaseFromFile()
        else if (strcmp (detect, "DUMP") == 0) {
            char dumpOutput[5000];
            char database[500];
            getDatabaseFromFile(player, "use_database.txt", database);
            handleDump(database, dumpOutput);
            // Mengirim respons ke client
            if (send(new_socket, dumpOutput, strlen(dumpOutput), 0) < 0) {
                perror("Gagal mengirim respons ke client");
                exit(EXIT_FAILURE);
        }


    }
}
```
Beberapa perintah yang diperiksa dan dijalankan antara lain adalah CREATE USER, CREATE DATABASE, CREATE TABLE, GRANT PERMISSION, USE, DROP DATABASE, DROP TABLE, DROP COLUMN, INSERT, UPDATE, DELETE, SELECT, EXIT, dan DUMP. Setiap perintah akan memanggil fungsi yang sesuai untuk menangani perintah tersebut.

Program dump database harus kita jalankan tiap jam untuk semua database dan log, kemudian kita zip sesuai dengan timestamp, kemudian log dikosongkan kembali. Berikut ini adalah file bash yang digunakan untuk menjalankan ketentuan yang diminta:
```bash
#!/bin/bash

backup_dir="/mnt/d/anneu/smt4/sisop/finalPraktikum/database/databases/"
runtime_dir="/mnt/d/anneu/smt4/sisop/finalPraktikum"
timestamp=$(date +\%Y-\%m-\%d_\%H:\%M:\%S)
zip_file="$timestamp.zip"

# Membuat file backup untuk setiap direktori anak
for dir in "$backup_dir"/*; do
  cd /mnt/d/anneu/smt4/sisop/finalPraktikum/ && \
  ./dump -u anew -p penyu "$(basename "$dir")" > "$backup_dir/$(basename "$dir").backup"
done

# Menyalin file log.txt ke direktori backup
cp "$runtime_dir/log.txt" "$backup_dir"

# Mengosongkan file log.txt yang asli
echo -n > "$runtime_dir/log.txt"

# Membuat file zip dari semua file backup dan log.txt dengan format timestamp
cd "$backup_dir" && zip "$zip_file" *.backup log.txt

# Menghapus file yang memiliki nama sama dengan file dalam zip
find "$backup_dir" -maxdepth 1 -type f ! -name "$zip_file" -delete

#0 * * * * /mnt/d/anneu/smt4/sisop/finalPraktikum/cron_db.sh

```

Skrip ini adalah skrip bash yang digunakan untuk membuat salinan cadangan (backup) dari direktori dan file log dalam sebuah database. Pertama, variabel backup_dir dan runtime_dir didefinisikan untuk menyimpan direktori backup dan direktori runtime skrip ini. Variabel timestamp digunakan untuk membuat nama unik berdasarkan waktu saat ini, dan variabel zip_file digunakan untuk menyimpan nama file zip yang akan dibuat.

Selanjutnya, skrip melakukan iterasi pada setiap direktori anak dalam backup_dir menggunakan loop for dir in "$backup_dir"/*. Di dalam loop, skrip berpindah ke direktori runtime_dir dan menjalankan skrip dump dengan argumen yang sesuai untuk membuat salinan cadangan dari setiap direktori anak. Salinan cadangan tersebut disimpan dalam file dengan ekstensi .backup di dalam direktori backup.

Setelah itu, skrip menyalin file log.txt ke direktori backup menggunakan cp. Kemudian, file log.txt yang asli dikosongkan menggunakan echo -n > "$runtime_dir/log.txt".

Selanjutnya, skrip berpindah ke direktori backup dan membuat file zip dengan nama yang berdasarkan timestamp dan mengemas semua file backup dan file log.txt di dalamnya menggunakan perintah zip.

Setelah file zip berhasil dibuat, skrip menghapus file yang memiliki nama yang sama dengan file dalam zip menggunakan perintah find dengan opsi -delete, kecuali file zip itu sendiri.

Skrip ini dapat diatur untuk dijalankan secara otomatis dengan menjadwalkannya menggunakan crontab. Pada contoh terakhir di skrip, terdapat komentar yang menunjukkan pengaturan jadwal crontab dengan menjalankan skrip ini setiap jam 0.



# Dump
```
//Mengimpor library yang diperlukan
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <arpa/inet.h>

//Mendefinisikan alamat IP dan port server
#define SERVER_IP "127.0.0.2"
#define SERVER_PORT 8889

int main(int argc, char* argv[]) {
    //membuat socket
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    //memeriksa apakah socket berhasil dibuat
    if (sock == -1) {
        perror("Gagal membuat socket");
        exit(EXIT_FAILURE);
    }

    //inisialisasi struktur alamat server
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(SERVER_PORT);
    if (inet_pton(AF_INET, SERVER_IP, &(server_address.sin_addr)) <= 0) {
        perror("Alamat server tidak valid");
        exit(EXIT_FAILURE);
    }

    //membuat koneksi ke server
    if (connect(sock, (struct sockaddr*)&server_address, sizeof(server_address)) < 0) {
        perror("Koneksi ke server gagal");
        exit(EXIT_FAILURE);
    }
    
    //membaca input pengguna
    char user[2000], password[2000];
    int option;

    while ((option = getopt(argc, argv, "u:p:")) != -1) {
        switch (option) {
            case 'u':
                strncpy(user, optarg, sizeof(user) - 1);
                break;
            case 'p':
                strncpy(password, optarg, sizeof(password) - 1);
                break;
            default:
                printf("Pilihan tidak valid\n");
                exit(EXIT_FAILURE);
        }
    }

    //mendapatkan nama database dari baris perintah
    char database[2000];
    // Get the database name from the command-line argument
    if (optind < argc) {
        strncpy(database, argv[optind], sizeof(database) - 1);
    } else {
        printf("Nama database tidak ditemukan\n");
        exit(EXIT_FAILURE);
    }

    //mengecek data user untuk autentikasi
    FILE* file = fopen("users.txt", "r");
    int userFound = 0;
    if (file == NULL) {
        system("touch users.txt");
        system("sudo chmod 777 users.txt");

        FILE* file2 = fopen("users.txt", "r");
        char line[100];
        while (fgets(line, sizeof(line), file2)) {
            line[strcspn(line, "\n")] = '\0';
            if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                userFound = 1;
                break;
            }
        }
        fclose(file2);
    } else {
        char line[100];
        while (fgets(line, sizeof(line), file)) {
            line[strcspn(line, "\n")] = '\0';
            if (strstr(line, user) != NULL && strstr(line, password) != NULL) {
                userFound = 1;
                break;
            }
        }

        fclose(file);
    }

    if (userFound == 0) {
        printf("User tidak ditemukan\n");
        exit(EXIT_FAILURE);
    } else {
        //gunakan command USE DATABASE untuk melihat apakah user memiliki akses terhadap database
        char cmdUseDB[12002];
        sprintf(cmdUseDB, "\"%s\" USE \"%s\"", user, database);
        if (send(sock, cmdUseDB, strlen(cmdUseDB), 0) < 0) {
            perror("Gagal mengirim data pengguna ke server");
            exit(EXIT_FAILURE);
        }

        // Send the DUMP command automatically
        char full_command[12002];
        sprintf(full_command, "\"%s\" DUMP", user);
        if (send(sock, full_command, strlen(full_command), 0) < 0) {
            perror("Gagal mengirim data pengguna ke server");
            exit(EXIT_FAILURE);
        }

        char server_response[2000];
        if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
            perror("Gagal menerima respons dari server");
            exit(EXIT_FAILURE);
        }

        //kata yang menandakan program harus keluar jika sudah selesai
        char* word = "DROP TABLE";

        while (1) {
            sprintf(full_command, "\"%s\" DUMP", user);
            if (send(sock, full_command, strlen(full_command), 0) < 0) {
                perror("Gagal mengirim data pengguna ke server");
                exit(EXIT_FAILURE);
            }

            if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
                perror("Gagal menerima respons dari server");
                exit(EXIT_FAILURE);
            }

            printf("%s\n", server_response);

            if (strstr(server_response, word) != NULL) {
                exit(1);
            } else {
                continue;
            }
        }
    }

    //menutup socket
    close(sock);

    return EXIT_SUCCESS;
}
```

```
// Membuat socket
int sock = socket(AF_INET, SOCK_STREAM, 0);
if (sock == -1) {
    perror("Gagal membuat socket");
    exit(EXIT_FAILURE);
}
```
Potongan kode di atas membuat soket dengan menggunakan socket(AF_INET, SOCK_STREAM, 0). Jika pembuatan soket gagal, maka akan dicetak pesan kesalahan dengan menggunakan perror("Gagal membuat socket") dan program akan keluar dengan status keluaran yang menunjukkan kegagalan.

```
struct sockaddr_in server_address;
server_address.sin_family = AF_INET;
server_address.sin_port = htons(SERVER_PORT);
if (inet_pton(AF_INET, SERVER_IP, &(server_address.sin_addr)) <= 0) {
    perror("Alamat server tidak valid");
    exit(EXIT_FAILURE);
}
```
Potongan kode di atas menginisialisasi struktur sockaddr_in yang akan digunakan untuk menentukan alamat server yang akan dikoneksi. SERVER_PORT adalah port yang ditentukan dan SERVER_IP adalah alamat IP server yang ditentukan. Jika konversi alamat IP gagal, maka akan dicetak pesan kesalahan dan program akan keluar dengan status keluaran yang menunjukkan kegagalan.
```
if (connect(sock, (struct sockaddr*)&server_address, sizeof(server_address)) < 0) {
    perror("Koneksi ke server gagal");
    exit(EXIT_FAILURE);
}
```
Potongan kode di atas membuat koneksi ke server dengan menggunakan connect(). Jika koneksi gagal, maka akan dicetak pesan kesalahan dan program akan keluar dengan status keluaran yang menunjukkan kegagalan.
```
while ((option = getopt(argc, argv, "u:p:")) != -1) {
    switch (option) {
        case 'u':
            strncpy(user, optarg, sizeof(user) - 1);
            break;
        case 'p':
            strncpy(password, optarg, sizeof(password) - 1);
            break;
        default:
            printf("Pilihan tidak valid\n");
            exit(EXIT_FAILURE);
    }
}
```
Potongan kode di atas menggunakan getopt() untuk membaca argumen baris perintah yang diberikan kepada program. Opsi yang valid adalah -u untuk nama pengguna dan -p untuk kata sandi. Jika opsi tidak valid diberikan, maka akan dicetak pesan kesalahan dan program akan keluar dengan status keluaran yang menunjukkan kegagalan. Nilai dari opsi -u akan disalin ke variabel user, sedangkan nilai opsi -p akan disalin ke variabel password.

```
FILE* file = fopen("users.txt", "r");
int userFound = 0;
if (file == NULL) {
    // ...
} else {
    // ...
}
```
Potongan kode di atas membuka file "users.txt" dengan menggunakan fopen() untuk memeriksa apakah file tersebut ada. Jika file tidak ada, maka akan dilakukan penanganan untuk membuat file tersebut. Jika file berhasil dibuka, maka akan dilakukan pembacaan baris per baris untuk mencari kecocokan nama pengguna dan kata sandi yang diberikan dengan yang ada di file. Jika kecocokan ditemukan, variabel userFound akan diatur menjadi 1.
```
sprintf(cmdUseDB, "\"%s\" USE \"%s\"", user, database);
if (send(sock, cmdUseDB, strlen(cmdUseDB), 0) < 0) {
    perror("Gagal mengirim data pengguna ke server");
    exit(EXIT_FAILURE);
}
```
Potongan kode di atas menggunakan sprintf() untuk memformat string perintah SQL untuk menggunakan database dengan nama yang ditentukan. String hasil format disimpan di dalam buffer cmdUseDB. Selanjutnya, send() digunakan untuk mengirim string tersebut melalui soket. Jika pengiriman gagal, maka akan dicetak pesan kesalahan dan program akan keluar dengan status keluaran yang menunjukkan kegagalan.
```
if (recv(sock, server_response, sizeof(server_response), 0) < 0) {
    perror("Gagal menerima respons dari server");
    exit(EXIT_FAILURE);
}
```
Potongan kode di atas menggunakan recv() untuk menerima respons dari server yang dikirim melalui soket. Data yang diterima akan disimpan di dalam buffer server_response. Jika penerimaan gagal, maka akan dicetak pesan kesalahan dan program akan keluar dengan status keluaran yang menunjukkan kegagalan.
```
sprintf(full_command, "\"%s\" DUMP", user);
if (send(sock, full_command, strlen(full_command), 0) < 0) {
    perror("Gagal mengirim data pengguna ke server");
    exit(EXIT_FAILURE);
}
```
Potongan kode di atas menggunakan sprintf() untuk memformat string perintah SQL untuk melakukan dump pada database. String hasil format disimpan di dalam buffer full_command. Selanjutnya, send() digunakan untuk mengirim string tersebut melalui soket. Jika pengiriman gagal, maka akan dicetak pesan kesalahan dan program akan keluar dengan status keluaran yang menunjukkan kegagalan.
```
if (strstr(server_response, word) != NULL) {
    exit(1);
} else {
    continue;
}
```
Potongan kode di atas menggunakan strstr() untuk mencari kata kunci "DROP TABLE" dalam respons dari server yang diterima. Jika kata kunci ditemukan, program akan keluar dengan status keluaran yang menunjukkan kegagalan (1). Jika kata kunci tidak ditemukan, program akan melanjutkan iterasi berikutnya dalam loop while.

# Dockerfile
```
FROM ubuntu:latest

# Install necessary packages
RUN apt-get update && \
    apt-get install -y gcc sudo

# Create a new user and set it as the working user
RUN useradd -ms /bin/bash myuser

# Set the working directory
WORKDIR /home/myuser

# Copy the source code
COPY ../database/database.c ./database/
COPY ../client/client.c ./client/

# Compile the programs
RUN gcc -pthread ./database/database.c -o ./database/database
RUN gcc ./client/client.c -o ./client/client

# Set the entry point to run the database and client programs
CMD ["bash", "-c", "./database/database & sleep 7 && ./client/client"]
```

# Docker-compose.yml
```
version: '3'

services:
  storage-app:
    image: keysanadea/storage-app:latest
    ports:
      - "8080-8105:80"
    deploy:
      replicas: 5
```

